/* Copyright 2018, Med Lucas.
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief
 */

/** \addtogroup
 ** @{ */

/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <string.h>
#include <time.h>

/*==================[macros and definitions]=================================*/

#define EXIT_SUCCESS 	0
#define FALSE			0
#define TRUE			!FALSE
#define BAUD_RATE_38400	38400

/* UART Registers */
#define UART0_BASE 			0x80000100
#define UART1_BASE 			0x80000900
#define UART2_BASE 			0x80000A00
#define UART3_BASE 			0x80000B00
#define UART0_DATA_REG		0x80000100
#define UART0_STAT_REG		0x80000104
#define UART0_CTRL_REG		0x80000108
#define UART0_SCLR_REG		0x8000010C

/* GPIO-0 registers */
#define GPIO0_DATA_REG		0x80000800
#define GPIO0_OUTPUT_REG	0x80000804
#define GPIO0_DIR_REG		0x80000808

/* GPTIMER Registers */
#define GPTIMER_SCALER_VALUE 			0x80000300
#define GPTIMER_SCALER_RELOAD_VALUE		0x80000304
#define GPTIMER_CONFIG_REG				0x80000308
#define TIMER1_COUNTER_VALUE_REG		0x80000310
#define TIMER1_RELOAD_VALUE_REG			0x80000314
#define TIMER1_CONTROL_REG				0x80000318
#define TIMER4_COUNTER_VALUE_REG		0x80000340
#define TIMER4_RELOAD_VALUE_REG			0x80000344
#define TIMER4_CONTROL_REG				0x80000348

#define	_V					volatile

typedef _V unsigned char 		uint8_t;
typedef _V unsigned short 		uint16_t;
typedef _V unsigned int 		uint32_t;

/* Configuración registros TIMER1 */
#define TIMER1_RELOAD_VALUE	0x0000ffff
#define TIMER1_INT_PENDING	~(1UL << 4)
#define TIMER1_INT_ENABLE	(1UL << 3)
#define TIMER1_LOAD			(1UL << 2)
#define TIMER1_RESTART		(1UL << 1)
#define TIMER1_ENABLE		(1UL << 0)

/* Configuración resgistros UART0 */
#define UART0_TX_ENABLE		(1UL << 1)
#define UART0_RX_ENABLE		(1UL << 0)
#ifdef BAUD_RATE_38400
#define UART0_BAUD_RATE		0x81
#endif
/*==================[internal data declaration]==============================*/




/*==================[internal functions declaration]=========================*/

/** @brief
 *  @param
 *	@return
 */
static void sleep(uint32_t t);

/** @brief
 * 	@param
 *  @return
 */
static void sleep_ms(uint32_t numberOfSeconds);

/*==================[internal data definition]===============================*/

/** @brief  */


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void sleep(uint32_t t)
{
	uint32_t milliSeconds = 1000 * t;

	while (milliSeconds != 0)
	{
		milliSeconds--;
	}

	return;
};

static void sleep_ms(uint32_t numberOfSeconds)
{
        /* Converting time into milli-seconds */
        uint32_t milliSeconds = 4 * 1000 * numberOfSeconds;

        while (milliSeconds != 0)
        {
                milliSeconds--;
        }
        return;
}
/*==================[external functions definition]==========================*/



int main(void)
{
	uint32_t* volatile uart0_base 	= (uint32_t*)UART0_DATA_REG;
	uint32_t* volatile uart0_stat 	= (uint32_t*)UART0_STAT_REG;
	uint32_t* volatile uart0_ctrl 	= (uint32_t*)UART0_CTRL_REG;
	uint32_t* volatile uart0_sclr	= (uint32_t*)UART0_SCLR_REG;
	uint32_t* volatile gpio0_data 	= (uint32_t*)GPIO0_DATA_REG;
	uint32_t* volatile gpio0_output = (uint32_t*)GPIO0_OUTPUT_REG;
	uint32_t* volatile gpio0_dir 	= (uint32_t*)GPIO0_DIR_REG;
	uint32_t* volatile timer1_reload_value = (uint32_t*)TIMER1_RELOAD_VALUE_REG;
	uint32_t* volatile timer1_control_reg = (uint32_t*)TIMER1_CONTROL_REG;
	unsigned int timer1_control_reg_val = *(uint32_t *)TIMER1_CONTROL_REG;

	/* Se habilita la interrupción del timer1 */
	*timer1_control_reg |= INT_ENABLE;

	/* Se configura el valor para el timer 1 */
	*timer1_reload_value = TIMER1_RELOAD_VALUE;

	/* Se configura la salida del GPIO. */
	*gpio0_dir = 0x0000e000;

	/* Se configura la salida de la UART. */
	*uart0_ctrl = UART0_TX_ENABLE | UART0_RX_ENABLE;

	/* Se configura el Baud Rate de la UART */
	*uart0_sclr = UART0_BAUD_RATE;

	/* Se escribe un 1 en la salida. */
	*uart0_base = 0x31;

	/* Se actualiza el Timer */
	*timer1_control_reg |= TIMER1_LOAD;

	while(1)
	{
		/* Prende y apaga el bit 16 del GPIO0
		 * cada 40 ms.
		 */
		*gpio0_output = 0x00008000;
		sleep_ms(40);
		*gpio0_output = 0x00000000;
		sleep_ms(40);

		if (timer1_control_reg_val & (unsigned int)TIMER1_INT_PENDING)
		{
			*gpio0_output = 0x0000e000;
			sleep_ms(40);
			/* Se actualiza el valor del timer */
			*timer1_control_reg |= (unsigned int)TIMER1_LOAD;
			/* Se limpia la interrupción */
			*timer1_control_reg &= (unsigned int)TIMER1_INT_PENDING;
		}

	}

	return(0);
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
