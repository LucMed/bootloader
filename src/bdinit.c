/* Sample bdinit routine to configure de processor on power-up */

/* Registros de configuración de las memorias.
 *
 */
#define MCFG1   0x80000000
#define MCFG2   0x80000004
#define MCFG3   0x80000008

#define GPIO0_DIR_REG	0x80000808
#define TIMER1_VAL		0x80000314

void bdinit0(void)
{
	return;
} /* bdinit0 */

void bdinit1(void)
{
	*(volatile unsigned int*)MCFG1 = 0x107801FF;
	*(volatile unsigned int*)MCFG2 = 0x81a07060;
	*(volatile unsigned int*)MCFG3 = 0x00137000;
	*(volatile unsigned int*)TIMER1_VAL = 0x0000ffff;
	return;
} /* bdinit1 */

void bdinit2(void)
{
	*(volatile unsigned int*)GPIO0_DIR_REG = 0x0000e000;
	return;
} /* bdinit2 */
